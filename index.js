const WiZLocalControl = require("wiz-local-control").default;

const detectedDevices = new Set();
const wizLocalControl = new WiZLocalControl({
  incomingMsgCallback: (msg, ip) => {
    console.log(`Received the message from WiZ Light ${JSON.stringify(msg)}`);
    if (!detectedDevices.has(ip)) {
      detectedDevices.add(ip);
      blink(ip, wizLocalControl);
      setInterval(() => blink(ip, wizLocalControl), 5000);
    }
  },
  interfaceName: "eth0"
});

function blink(ip, localControl) {
  localControl.changeStatus(false, ip);
  setTimeout(() => localControl.changeStatus(true, ip), 3000);
}

wizLocalControl.startListening();
