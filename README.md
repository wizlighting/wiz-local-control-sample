# Sample app for WiZ Local Control Lib

Sample app for the WiZ Local Control Lib.

It does the following:

1. Scans for all lights in the local network
2. Outputs communication messages in console
3. Turns the lights on and off with 5 seconds interval

Full protocol can be found in the main lib repo https://gitlab.com/wizlighting/wiz-local-control
